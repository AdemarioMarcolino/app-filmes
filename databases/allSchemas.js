import Realm from 'realm';

export const FILMES = "Filmes";

export const FilmeSchema = {
    name:FILMES,
    primaryKey:'id_filme',
    properties:{
        id_filme:'int',
        Title:{type:'string',default:false},
        Year:{type:'string',default:false},
        Genre:{type:'string',default:false},
        Director:{type:'string',default:false},
        Poster:{type:'string',default:false},
        imdbRating:{type:'string',default:false},
        Type:{type:'string',default:false},
        Plot:{type:'string',default:false},

    }
};
const databaseOptions ={
    path: 'filmesApp.realm',
    schema: [FilmeSchema],
    schemaVersion:0.1,
}

export const insertNewFilme = newFilme => new Promise((resolve, reject) =>{
    Realm.open(databaseOptions).then(realm=>{
        realm.write(() =>{
            realm.create(FILMES, newFilme);
            resolve(newFilme);
        })

    }).catch((error) =>reject(error));
});
export const updateFilme = filme => new Promise((resolve, reject) =>{
    Realm.open(databaseOptions).then(realm => {
        realm.write(()=>{
            let updatingFilme = realm.objectForPrimaryKey(FILME, filme.id_filme);
            updateFilme.Title = filme.Title;
            resolve();
        })
    }).catch((error) =>reject(error));
})
export const deleteFilme = filmeId_filme => new Promise((resolve, reject) =>{
    Realm.open(databaseOptions).then(realm => {
        realm.write(()=>{
            let deletingFilme = realm.objectForPrimaryKey(FILME, filmeId_filme);
            realm.delete(deletingFilme);
            resolve();
        })
    }).catch((error) =>reject(error));
})
export const allFilmes = () => new Promise((resolve, reject) =>{
    Realm.open(databaseOptions).then(realm => {
            let allFilmes = realm.objects(FILMES);
            resolve(allFilmes);
    }).catch((error) => reject(error));
})
export default new Realm(databaseOptions);