/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {TextInput,FlatList, StyleSheet, Text, View, ActivityIndicator, Image, Dimensions, TouchableOpacity, Modal} from 'react-native';
import {Header} from 'react-native-elements';
import SpinnerButton from 'react-native-spinner-button';
import realm from '../databases/allSchemas';
import { insertNewFilme } from '../databases/allSchemas';
import { updateFilme,deleteFilme, allFilmes,  } from '../databases/allSchemas';



import t from 'tcomb-form-native'; // 0.6.9

let {width} = Dimensions.get('window')

let numberGrid = 2
let widthGrid = width / numberGrid

const Form = t.form.Form;
const Filme = t.struct({
  nomeDoFilme: t.String,
});

//BANCO SQLITE IMPORT 
var SQLite = require('react-native-sqlite-storage');
var db = SQLite.openDatabase({name:'film.db', createFromLocation:'~filmes.db'})

export default class App extends Component {
  constructor(props){
    super(props);
    this.state ={ 
      isLoading: true,
      showMe:false,
      text: '',
      filmes:[],
      defaultLoading: false,
    }
    this.reloadData();
        realm.addListener('change', () => {
            this.reloadData();
        })
    
}

reloadData = () => {
  allFilmes().then((filmes) => {
      this.setState({dataSource:filmes});
  }).catch((error) => {
      this.setState({dataSource:[]});
  })
  console.log('realmDara');
}
  

 componentDidMount(){
   this.reloadData();

   allFilmes().then((filmes) => {
      this.setState({dataSource:filmes});
  }).catch((error) => {
    alert(error)

      this.setState({dataSource:[]});
  })
  console.log('realmDara');
    
  }

  //FUNÇÃO PARA BUSCAR FILME 
  buscarfilme = async ({term})=>{
    await fetch(`http://www.omdbapi.com/?apikey=b515063&t=${term}`)
    .then((response)=>response.json())
    .then((responseJson)=>{
      filmArray = {"movie":[responseJson]};
      this.setState({
        isLoading:false,
      },function(){
        alert(JSON.stringify(filmArray.movie[0]))
        let fil = filmArray.movie[0].Title;
        let year = filmArray.movie[0].Year;
        let gender = filmArray.movie[0].Genre;
        let director = filmArray.movie[0].Director;
        let poster = filmArray.movie[0].Poster;
        let rating = filmArray.movie[0].imdbRating;
        let type = filmArray.movie[0].Type;
        let plot = filmArray.movie[0].Plot;

        const newFilme = {
          id_filme: Math.floor(Date.now() / 100),
          Title:fil,
          Year:year,
          Genre:gender,
          Director:director,
          Poster:poster,
          imdbRating:rating,
          Type:type,
          Plot:plot
      };
      insertNewFilme(newFilme)
      .then(
        alert("filme salvo")
      )
      .catch((error) =>{
          alert(`Erro ao inserir o filme ${error}`);
      })
        this.setState({ defaultLoading: false });
      });
    })
    .catch((error)=>{
      alert(JSON.stringify(error));
      this.setState({ defaultLoading: false });
    });
  }

  // RETORNA IMAGENS DOS FILMES NA PAGINA INICIAL
  renderItem = ({item}) => {
    return <View>
    <Image source={{uri: item.Poster}} style={styles.itemImage} />
    </View>
   }

  // FUNÇÃO PARA BUSCAR E SALVAR O FILME
   addFilme = ()=>{
    let term = this.state.text;
    this.buscarfilme({term})
  }

   render(){
    return(
      <View style={styles.container}>
      <Header
        leftComponent={{ icon: 'menu', color: '#fff' }}
        centerComponent={{ text: 'Meus Filmes', style: { color: '#fff' } }}
        rightComponent={{ icon: 'home', color: '#fff' }}
      />
        {/* LISTA DE FILMES CADASTRADOS */}
        <FlatList
          data={this.state.dataSource}
          renderItem={({item}) => <Text>{item.Title}, {item.Year}</Text>}
          numColumns={2}
          renderItem={this.renderItem}
          renderButton={this.renderButton}
          keyExtractor={({imdbRating}, index) => imdbRating}
        />
        {/* LISTA DE FILMES CADASTRADOS */}

        <TouchableOpacity style={styles.btn}
            onPress={()=>{
                  this.setState({
                    showMe:true
                  })
                }}>
          <Text style={styles.plus}>+</Text>
        </TouchableOpacity>

        {/* MODAL PARA BUSCAR E CADASTRAR NOVO FILME */}
        <Modal 
        visible={this.state.showMe} 
        onRequestClose={()=>this.setState({
            showMe:false
        })}>
        <Header
        leftComponent={{ icon: 'menu', color: '#fff' }}
        centerComponent={{ text: 'Novo Filme', style: { color: '#fff' } }}
        rightComponent={{ icon: 'home', color: '#fff' }}
      />
            <View style={styles.modalView}>
            
                <TouchableOpacity onPress={()=>{
                  this.setState({
                    showMe:false
                  })
                }}>
                </TouchableOpacity>
                {/* INPUT PARA BUSCA DO FILME */}
                <TextInput
                  style={{height: 40,width:350, borderColor: 'gray', borderBottomWidth: 0.75}}
                  onChangeText={(text) => this.setState({text})}
                  value={this.state.text}
                  placeholder="Digite o nome do Filme"
                />
                {/* INPUT PARA BUSCA DO FILME */}

                {/* BOTÃO PARA BUSCAR FILMES */}
                <SpinnerButton
                  buttonStyle={styles.buttonStyle}
                  isLoading={this.state.defaultLoading}
                  onPress={() => {
                    this.addFilme();
                    this.setState({ defaultLoading: true });
                  }}
                  indicatorCount={2}
                  >
                  <Text style={styles.buttonText}>Buscar Filme</Text>
                </SpinnerButton>
                
            </View>
        </Modal>
        {/* MODAL PARA BUSCAR E CADASTRAR NOVO FILME */}
      </View>
      
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "space-between"
  },
  itemImage:{
    width: widthGrid,
    height: widthGrid,
    marginBottom:5
  },
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: 'white',
  },
  btn:{
    position:'absolute',
    width:70,height:70,
    backgroundColor:'#ff8923',
    borderRadius:50,
    bottom:10,right:10,
    alignItems:'center',
    justifyContent:'center'
  },
  plus:{
    color:'white',
    fontSize:35
  },
  textSave:{
    fontSize:23,
    fontFamily:"roboto",
    color:'white'
  },
  modalView:{
    height:350,
    justifyContent:'center',
    alignItems:'center',
  },
  closeText:{
    backgroundColor:'#333',
    color:'#bbb',
    padding:5,
    margin:20
  },
  form:{
    borderColor:'red',
    borderWidth:29
  },
  btNovo:{
    position:'absolute',
    width:200,height:50,
    borderRadius:20,
    backgroundColor:'#1194f6',
    bottom:10,
    alignItems:'center',
    justifyContent:'center'
  },
  buttonText: {
    fontSize: 20,
    textAlign: 'center',
    color: 'white',
    paddingHorizontal: 20,
  },
  buttonStyle: {
    borderRadius: 10,
    margin: 20,
  },
  itemIm: {
    alignItems: "center",
    backgroundColor: "#dcda48",
    flexGrow: 1,
    margin: 4,
    padding: 20
  },
  
});
